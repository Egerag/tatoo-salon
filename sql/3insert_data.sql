INSERT INTO user (id, role, first_name, last_name, login, password, discount)
VALUES
  (1, 0, 'Valentin', 'Danilov', 'Garbage', '1234', 10),
  (2, 1, 'Egor', 'Chetsov', 'Collector', 'qwer', 35),
  (3, 0, 'Putin', 'Vladimir', 'Pre$1dent', 'krim', 8),
  (4, 0, 'Maria', 'Petrova', 'Ch1kaP1ka', '2233', 18);

INSERT INTO tattoo (size, color, image, price, author_id)
VALUES
  (20, 'orange', 'images/flowers.jpg', 120, 2),
  (10, 'red', 'images/phoenix.jpg', 60, 2),
  (15, 'black', 'images/spider.jpg', 90, 2),
  (80, 'black', 'images/strips.jpg', 480, 2);

INSERT INTO balance (user_id, current_balance, overdruft)
VALUES
  (1, 800, 150),
  (2, 450, 350),
  (3, 200, 100),
  (4, 760, 200);
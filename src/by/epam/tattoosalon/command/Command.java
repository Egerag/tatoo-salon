package by.epam.tattoosalon.command;

import by.epam.tattoosalon.entity.Role;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

public interface Command {

    List<Role> getRoles();

    void execute(HttpServletRequest request, HttpServletResponse response);
}

package by.epam.tattoosalon.command.implementation;

import by.epam.tattoosalon.command.Command;
import by.epam.tattoosalon.command.common.CommandCommon;
import by.epam.tattoosalon.command.common.JSPPath;
import by.epam.tattoosalon.entity.Balance;
import by.epam.tattoosalon.entity.Role;
import by.epam.tattoosalon.entity.Tattoo;
import by.epam.tattoosalon.entity.User;
import by.epam.tattoosalon.exceptions.ConstantException;
import by.epam.tattoosalon.service.BalanceService;
import by.epam.tattoosalon.service.TattooService;
import by.epam.tattoosalon.service.UserService;
import by.epam.tattoosalon.service.implementation.BalanceServiceImplementation;
import by.epam.tattoosalon.service.implementation.TattooServiceImplementation;
import by.epam.tattoosalon.service.implementation.UserServiceImplementation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import sun.misc.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddTattoo implements Command {

    private static final Logger LOG = LogManager.getLogger("name");

    List<Role> roles;

    public AddTattoo() {
        roles = Arrays.asList(Role.USER, Role.ADMINISTRATOR);
    }

    @Override
    public List<Role> getRoles() {
        return roles;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {

        TattooService tattooService = new TattooServiceImplementation();

        Integer size = Integer.valueOf(request.getParameter("size"));
        String color = request.getParameter("color");
        Integer price = Integer.valueOf(request.getParameter("price"));
        try {
            Part filePart = request.getPart("file");
            String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
            InputStream fileContent = filePart.getInputStream();
            byte[] bytes = new byte[fileContent.available()];
            fileContent.read(bytes);
            User user = (User) request.getSession().getAttribute("User");
            File file = new File("C:\\epam_crew\\final_TatooSalon\\out\\artifacts\\final_TatooSalon_war_exploded\\userimg\\" + fileName);
            OutputStream outputStream = new FileOutputStream(file);
            outputStream.write(bytes);
            tattooService.addTattoo(new Tattoo(size,
                    color,
                    "./userimg/" + fileName,
                    price,
                    user));
            if (user.getDiscount() < 0.5) {
                UserService userService = new UserServiceImplementation();
                double newDiscount = new BigDecimal(user.getDiscount() + 0.05).setScale(3, RoundingMode.FLOOR).doubleValue();
                user.setDiscount(newDiscount);
                request.getSession().setAttribute("User", user);
                userService.setData(user);
            }
            request.getSession().setAttribute("success", "Tattoo was added");
            response.sendRedirect("profile.html");
        } catch (IOException | ServletException | SQLException | ConstantException e) {
            LOG.error(e);
        }


    }
}

package by.epam.tattoosalon.command.implementation;

import by.epam.tattoosalon.command.Command;
import by.epam.tattoosalon.command.common.CommandCommon;
import by.epam.tattoosalon.command.common.JSPPath;
import by.epam.tattoosalon.entity.Role;
import by.epam.tattoosalon.entity.Tattoo;
import by.epam.tattoosalon.exceptions.ConstantException;
import by.epam.tattoosalon.service.TattooService;
import by.epam.tattoosalon.service.implementation.TattooServiceImplementation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class MainPage implements Command {

    private static final Logger LOG = LogManager.getLogger("name");

    List<Role> roles;

    public MainPage() {
        roles = Arrays.asList(Role.USER, Role.ADMINISTRATOR, Role.ANONYMOUS);
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        if (request.getSession().getAttribute("fail") != null) {
            request.setAttribute("fail", request.getSession().getAttribute("fail"));
            request.getSession().setAttribute("fail", null);
        }
        if (request.getSession().getAttribute("credit") != null) {
            request.setAttribute("credit", request.getSession().getAttribute("credit"));
            request.getSession().setAttribute("credit", null);
        }
        if (request.getSession().getAttribute("paid") != null) {
            request.setAttribute("paid", request.getSession().getAttribute("paid"));
            request.getSession().setAttribute("paid", null);
        }
        int onePageContent = 8;
        Double startIndex;
        if (request.getParameter("start") == null) {
            startIndex = 0.0;
        } else {
            startIndex = Double.valueOf(request.getParameter("start"));
        }
        //System.out.println("str ind" + startIndex);

        CommandCommon common = new CommandCommon();
        TattooService tattooService = new TattooServiceImplementation();
        Integer index;
        if (request.getParameter("index") == null) {
            index = 0;
        } else {
            index = Integer.valueOf(request.getParameter("index"));
        }
        startIndex+=index;
        List<Tattoo> tattooList;
        try {
            if (request.getAttribute("search") == null) {
                tattooList = tattooService.takeAllTattoos();
            } else {
                tattooList = (List<Tattoo>) request.getAttribute("search");
                startIndex = 0.0;
            }
            if ((tattooList.size() - index*onePageContent) < 8) {
                onePageContent = tattooList.size() - index*onePageContent;
            }
            request.setAttribute("startIndex", startIndex*8);
            request.setAttribute("content", onePageContent);
            request.setAttribute("tattoos", tattooList);
            request.setAttribute("size", tattooList.size());
            common.visitPage(request, response, JSPPath.MAIN_USER_PAGE.getUrl());
        } catch (SQLException | ServletException | IOException | ConstantException e) {
            LOG.error(e);
        }
    }

    @Override
    public List<Role> getRoles() {
        return roles;
    }
}

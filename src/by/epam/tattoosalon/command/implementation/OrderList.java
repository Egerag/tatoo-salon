package by.epam.tattoosalon.command.implementation;

import by.epam.tattoosalon.command.Command;
import by.epam.tattoosalon.command.common.CommandCommon;
import by.epam.tattoosalon.command.common.JSPPath;
import by.epam.tattoosalon.entity.Order;
import by.epam.tattoosalon.entity.Role;
import by.epam.tattoosalon.entity.Tattoo;
import by.epam.tattoosalon.entity.User;
import by.epam.tattoosalon.exceptions.ConstantException;
import by.epam.tattoosalon.service.OrderService;
import by.epam.tattoosalon.service.implementation.OrderServiceImplementation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class OrderList implements Command {

    private static final Logger LOG = LogManager.getLogger("name");

    List<Role> roles;

    public OrderList() {
        roles = Arrays.asList(Role.USER, Role.ADMINISTRATOR);
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        CommandCommon common = new CommandCommon();
        OrderService orderService = new OrderServiceImplementation();
        try {
            Integer sum = orderService.calculateTotalPrice(((Order) request.
                    getSession().getAttribute("Order")).getIdentity());
            List<Tattoo> order = orderService.takeAllTattoos(((Order)request.
                    getSession().getAttribute("Order")).getIdentity());
            request.setAttribute("order", order);
            User user = (User) request.getSession().getAttribute("User");
            request.setAttribute("sum", sum*(1 - user.getDiscount()));
            common.visitPage(request, response, JSPPath.ORDER_LIST.getUrl());
        } catch (SQLException | ServletException | IOException | ConstantException e) {
            LOG.error(e);
        }

    }

    @Override
    public List<Role> getRoles() {
        return roles;
    }
}

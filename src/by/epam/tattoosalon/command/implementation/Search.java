package by.epam.tattoosalon.command.implementation;

import by.epam.tattoosalon.command.Command;
import by.epam.tattoosalon.command.common.CommandCommon;
import by.epam.tattoosalon.entity.Role;
import by.epam.tattoosalon.entity.Tattoo;
import by.epam.tattoosalon.exceptions.ConstantException;
import by.epam.tattoosalon.service.TattooService;
import by.epam.tattoosalon.service.implementation.TattooServiceImplementation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class Search implements Command {

    private static final Logger LOG = LogManager.getLogger("name");

    List<Role> roles;

    public Search() {
        roles = Arrays.asList(Role.USER, Role.ADMINISTRATOR, Role.ANONYMOUS);
    }

    @Override
    public List<Role> getRoles() {
        return roles;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        String color = request.getParameter("color");
        TattooService tattooService = new TattooServiceImplementation();
        CommandCommon common = new CommandCommon();
        try {
            List<Tattoo> search = tattooService.searchByColor(color);
            if(search.size() > 0) {
                request.setAttribute("search", search);
            } else {
                request.setAttribute("search", null);
                request.setAttribute("fail", "Not found");
            }
            request.setAttribute("start", 0);
            request.setAttribute("index", null);
            request.setAttribute("command", "main_page");
            common.visitPage(request, response, "usermain.html");
        } catch (SQLException | IOException | ServletException | ConstantException e) {
            LOG.error(e);
        }
    }
}

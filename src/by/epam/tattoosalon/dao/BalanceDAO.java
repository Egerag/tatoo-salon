package by.epam.tattoosalon.dao;

import by.epam.tattoosalon.entity.Balance;
import by.epam.tattoosalon.exceptions.ConstantException;

public interface BalanceDAO extends DAOBase<Balance> {

    Balance findByOwner (Integer userID) throws ConstantException;
}

package by.epam.tattoosalon.dao;

import by.epam.tattoosalon.entity.BasicEntity;
import by.epam.tattoosalon.exceptions.ConstantException;

public interface DAOBase<Type extends BasicEntity> {

    Integer create(Type entity) throws ConstantException;

    Type findByID(Integer identity) throws ConstantException;

    void update(Type entity) throws ConstantException;

    void delete(Integer identity) throws ConstantException;
}

package by.epam.tattoosalon.dao;

import by.epam.tattoosalon.entity.Order;
import by.epam.tattoosalon.entity.Tattoo;
import by.epam.tattoosalon.exceptions.ConstantException;

import java.util.List;

public interface OrderDAO extends DAOBase<Order> {

    List<Tattoo> findAllTattoos (Integer orderID) throws ConstantException;

    Integer countTotalPrice (Integer orderID) throws ConstantException;

    Integer addTattoo (Integer orderID, Integer tattooID) throws ConstantException;

    List<Order> findByUser (Integer userID) throws ConstantException;

    void deleteTattoo (Integer orderID,Integer tattooID) throws ConstantException;

    Order findActiveOrder(Integer userID) throws ConstantException;

    List<Order> findInactiveOrder(Integer userID) throws ConstantException;

}

package by.epam.tattoosalon.dao;

import by.epam.tattoosalon.entity.Tattoo;
import by.epam.tattoosalon.exceptions.ConstantException;

import java.util.List;

public interface TattooDAO extends DAOBase<Tattoo> {

    List<Tattoo> findAllTattoo () throws ConstantException;

    List<Tattoo> findSmallerThan (Integer maxSize) throws ConstantException;

    List<Tattoo> cheaperThan (Integer maxPrice) throws ConstantException;

    List<Tattoo> findByColor (String color) throws ConstantException;

    List<Tattoo> findByAuthor (Integer authorID) throws ConstantException;
}

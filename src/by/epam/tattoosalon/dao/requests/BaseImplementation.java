package by.epam.tattoosalon.dao.requests;

import by.epam.tattoosalon.dao.connection.ConnectionCompare;
import by.epam.tattoosalon.dao.connection.PoolConnection;
import by.epam.tattoosalon.exceptions.ConstantException;

import java.sql.Connection;
import java.sql.SQLException;

abstract class BaseImplementation {

    protected ConnectionCompare connection;

    public void setConnection() throws ConstantException {
        try {
            this.connection = PoolConnection.getInstance().getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            throw new ConstantException(e);
        }
    }

    public void closeConnection() throws SQLException, ConstantException {
        PoolConnection.getInstance().freeConnection(this.connection);}
}

package by.epam.tattoosalon.dao.requests;

import by.epam.tattoosalon.dao.TattooDAO;
import by.epam.tattoosalon.entity.Tattoo;
import by.epam.tattoosalon.entity.User;
import by.epam.tattoosalon.exceptions.ConstantException;
import by.epam.tattoosalon.service.UserService;
import by.epam.tattoosalon.service.implementation.UserServiceImplementation;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class TattooDAOImplementation extends BaseImplementation implements TattooDAO {

    private User returnAuthor(Integer authorID) throws SQLException, ClassNotFoundException, ConstantException {
        UserService user = new UserServiceImplementation();
        return user.searchByID(authorID);
    }

    @Override
    public List<Tattoo> findAllTattoo() throws ConstantException {
        String sql = "SELECT id, size, color, image, price, author_id FROM tattoo";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Tattoo> tattoos = new ArrayList<>();
        try {
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                User user = returnAuthor(resultSet.getInt(6));
                tattoos.add(new Tattoo(resultSet.getInt(1),
                        resultSet.getInt(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getInt(5),
                        user));
            }
            return tattoos;
        } catch (SQLException | ClassNotFoundException | ConstantException e) {
            throw new ConstantException(e);
        } finally {
            try {
                assert resultSet != null;
                resultSet.close();
            } catch (SQLException e) {
            }
            try {
                statement.close();
            } catch (SQLException e) {
            }
        }
    }

    @Override
    public List<Tattoo> findSmallerThan(Integer maxSize) throws ConstantException {
        String sql = "SELECT id, size, color, image, price, author_id FROM tattoo WHERE size<?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Tattoo> tattoos = new ArrayList<>();
        User author;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,maxSize);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                author = returnAuthor(resultSet.getInt(6));
                tattoos.add(new Tattoo(resultSet.getInt(1),
                        resultSet.getInt(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getInt(5),
                        author));
            }
            return tattoos;
        } catch (SQLException | ClassNotFoundException | ConstantException e) {
            throw new ConstantException(e);
        } finally {
            try {
                assert resultSet != null;
                resultSet.close();
            } catch (SQLException e) {
            }
            try {
                statement.close();
            } catch (SQLException e) {
            }
        }
    }

    @Override
    public List<Tattoo> cheaperThan(Integer maxPrice) throws ConstantException {
        String sql = "SELECT id, size, color, image, price, author_id FROM tattoo WHERE price<?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Tattoo> tattoos = new ArrayList<>();
        User author;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,maxPrice);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                author = returnAuthor(resultSet.getInt(6));
                tattoos.add(new Tattoo(resultSet.getInt(1),
                        resultSet.getInt(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getInt(5),
                        author));
            }
            return tattoos;
        } catch (SQLException e) {
            throw new ConstantException(e);
        } catch (ClassNotFoundException e) {
            throw new ConstantException(e);
        } finally {
            try {
                assert resultSet != null;
                resultSet.close();
            } catch (SQLException e) {
            }
            try {
                statement.close();
            } catch (SQLException e) {
            }
        }
    }

    @Override
    public List<Tattoo> findByColor(String color) throws ConstantException {
        String sql = "SELECT id, size, color, image, price, author_id FROM tattoo WHERE color=?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Tattoo> tattoos = new ArrayList<>();
        User author;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1,color);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                author = returnAuthor(resultSet.getInt(6));
                tattoos.add(new Tattoo(resultSet.getInt(1),
                        resultSet.getInt(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getInt(5),
                        author));
            }
            return tattoos;
        } catch (SQLException | ClassNotFoundException e) {
            throw new ConstantException(e);
        } finally {
            try {
                assert resultSet != null;
                resultSet.close();
            } catch (SQLException e) {
            }
            try {
                statement.close();
            } catch (SQLException e) {
            }
        }
    }

    @Override
    public List<Tattoo> findByAuthor(Integer authorID) throws ConstantException {
        String sql = "SELECT id, size, color, image, price, author_id FROM tattoo WHERE author_id<?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Tattoo> tattoos = new ArrayList<>();
        User author;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,authorID);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                author = returnAuthor(resultSet.getInt(6));
                tattoos.add(new Tattoo(resultSet.getInt(1),
                        resultSet.getInt(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getInt(5),
                        author));
            }
            return tattoos;
        } catch (SQLException | ClassNotFoundException e) {
            throw new ConstantException(e);
        } finally {
            try {
                assert resultSet != null;
                resultSet.close();
            } catch (SQLException e) {
            }
            try {
                statement.close();
            } catch (SQLException e) {
            }
        }
    }

    @Override
    public Integer create(Tattoo tattoo) throws ConstantException {
        String sql = "INSERT INTO tattoo (size, color, image, price, author_id) VALUES (?,?,?,?,?)";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, tattoo.getSize());
            statement.setString(2, tattoo.getColor());
            statement.setString(3, tattoo.getImage());
            statement.setInt(4, tattoo.getPrice());
            statement.setInt(5, tattoo.getAuthor().getIdentity());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if(resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                return -1;
            }
        } catch(SQLException e) {
            throw new ConstantException(e);
        } finally {
            try {
                assert resultSet != null;
                resultSet.close();
            } catch(SQLException | NullPointerException e) {
            }
            try {
                statement.close();
            } catch(SQLException | NullPointerException e) {
            }
        }
    }

    @Override
    public Tattoo findByID(Integer identity) throws ConstantException {
        String sql = "SELECT id, size, color, image, price, author_id FROM tattoo WHERE id=?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Tattoo> tattoos = new ArrayList<>();
        User author;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,identity);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                author = returnAuthor(resultSet.getInt(6));
                return new Tattoo(resultSet.getInt(1),
                        resultSet.getInt(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getInt(5),
                        author);
            } else {
                return null;
            }
        } catch (SQLException | ClassNotFoundException e) {
            throw new ConstantException(e);
        } finally {
            try {
                assert resultSet != null;
                resultSet.close();
            } catch (SQLException e) {
            }
            try {
                statement.close();
            } catch (SQLException e) {
            }
        }
    }

    @Override
    public void update(Tattoo tattoo) throws ConstantException {
        String sql = "UPDATE tattoo SET size=?, color=?, image=?, price=?, author_id=? WHERE id=?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, tattoo.getSize());
            statement.setString(2, tattoo.getColor());
            statement.setString(3, tattoo.getImage());
            statement.setInt(4, tattoo.getPrice());
            statement.setInt(5, tattoo.getAuthor().getIdentity());
            statement.setInt(6, tattoo.getIdentity());
            statement.executeUpdate();
        } catch(SQLException e) {
            throw new ConstantException(e);
        } finally {
            try {
                assert statement != null;
                statement.close();
            } catch(SQLException | NullPointerException e) {
            }
        }
    }

    @Override
    public void delete(Integer identity) throws ConstantException {
        String sql = "DELETE FROM tattoo WHERE id=?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, identity);
            statement.executeUpdate();
        } catch(SQLException e) {
            throw new ConstantException(e);
        } finally {
            try {
                assert statement != null;
                statement.close();
            } catch(SQLException | NullPointerException e) {
            }
        }
    }
}

package by.epam.tattoosalon.run;

import by.epam.tattoosalon.dao.connection.PoolConnection;
import by.epam.tattoosalon.entity.Tattoo;
import by.epam.tattoosalon.exceptions.ConstantException;
import by.epam.tattoosalon.service.TattooService;
import by.epam.tattoosalon.service.implementation.TattooServiceImplementation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    private static final Logger LOG = LogManager.getLogger("name");

    public static void main(String[] args) throws SQLException, ClassNotFoundException, ConstantException {

        /*
        BalanceService balanceService = new BalanceServiceImplementation();
        Balance balance = balanceService.searchByOwner(1);
        System.out.println(balance.getOverdraft());
        UserService userService = new UserServiceImplementation();
        User user = userService.searchByPersonal("Garbage", "1234").get(0);
        System.out.println(user.getDiscount());
        TattooService tattooService = new TattooServiceImplementation();
        List<Tattoo> tattoos = tattooService.takeAllTattoos();

        Integer id = 3;
        String role = "USER";
        Double discount = 8.0;
        Double overdraft = 100.0;
        Double newBalance = 200.0;
        BalanceService balanceService = new BalanceServiceImplementation();
        UserService userService = new UserServiceImplementation();
        try {
            Balance balance = balanceService.searchByID(id);
            User user = balance.getUser();
            if (Objects.equals(role, "USER")) {
                user.setRole(Role.USER);
            } else {
                user.setRole(Role.ADMINISTRATOR);
            }
            user.setDiscount(discount/100.0);
            balance.setOverdraft(overdraft);
            balance.setCurrentBalance(newBalance);
            userService.setData(user);
            balanceService.setData(balance);

        } catch (SQLException e) {
            e.printStackTrace();
        }


        CommandCommon common = new CommandCommon();
        OrderService orderService = new OrderServiceImplementation();
        Integer tattooID = 7;
        UserService userService = new UserServiceImplementation();
        try {
            if (orderService.searchActiveOrder(1) == null) {
                orderService.addOrder(new Order(false, 0,
                        new Date(Calendar.getInstance().getTime().getTime()),
                        userService.searchByID(1)));
            }
            Order order = orderService.searchActiveOrder(1);
            orderService.addTattooOrder(order.getIdentity(), tattooID);
        } catch (SQLException e) {
            e.printStackTrace();
        }


        OrderService orderService = new OrderServiceImplementation();
        int summ = orderService.calculateTotalPrice(17);
        List<Tattoo> tattoos = orderService.takeAllTattoos(17);
        System.out.println(summ);
        */

        PoolConnection.getInstance().init(1000, 0, 10);
        TattooService tattooService = new TattooServiceImplementation();
        List<Tattoo> tattoos = new ArrayList<>();
        tattoos = tattooService.searchByColor("black");
        System.out.println(tattoos.size());
        LOG.info("im working");

    }
}

package by.epam.tattoosalon.service;

import by.epam.tattoosalon.entity.Balance;
import by.epam.tattoosalon.exceptions.ConstantException;

import java.sql.SQLException;

public interface BalanceService {

    Balance searchByOwner(Integer userID) throws SQLException, ConstantException;

    Integer addBalance(Balance balance) throws SQLException, ConstantException;

    Balance searchByID(Integer identity) throws SQLException, ConstantException;

    void setData(Balance balance) throws SQLException, ConstantException;

    void deleteBYID(Integer identity) throws SQLException, ConstantException;
}

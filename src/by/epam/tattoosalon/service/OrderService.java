package by.epam.tattoosalon.service;

import by.epam.tattoosalon.entity.Order;
import by.epam.tattoosalon.entity.Tattoo;
import by.epam.tattoosalon.exceptions.ConstantException;

import java.sql.SQLException;
import java.util.List;

public interface OrderService {

    List<Tattoo> takeAllTattoos (Integer orderID) throws SQLException, ConstantException;

    Integer calculateTotalPrice(Integer orderID) throws SQLException, ConstantException;

    List<Order> searchByUsers(Integer userID) throws SQLException, ConstantException;

    Order searchActiveOrder(Integer userID) throws SQLException, ConstantException;

    List<Order> searchInactiveOrder(Integer userID) throws SQLException, ConstantException;

    Integer addOrder(Order order) throws SQLException, ConstantException;

    Integer addTattooOrder(Integer orderID, Integer tattooID) throws SQLException, ConstantException;

    Order searchByID(Integer identity) throws SQLException, ConstantException;

    void setData(Order order) throws SQLException, ConstantException;

    void deleteByID(Integer identity) throws SQLException, ConstantException;

    void deleteTattoo(Integer orderID, Integer tattooID) throws SQLException, ConstantException;
}

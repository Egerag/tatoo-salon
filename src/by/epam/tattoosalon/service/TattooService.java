package by.epam.tattoosalon.service;

import by.epam.tattoosalon.entity.Tattoo;
import by.epam.tattoosalon.exceptions.ConstantException;

import java.sql.SQLException;
import java.util.List;

public interface TattooService {

    List<Tattoo> takeAllTattoos() throws SQLException, ConstantException;

    List<Tattoo> searchSmallerThan(Integer maxSize) throws SQLException, ConstantException;

    List<Tattoo> searchCheaperThan(Integer maxPrice) throws SQLException, ConstantException;

    List<Tattoo> searchByColor(String color) throws SQLException, ConstantException;

    List<Tattoo> searchByAuthor(Integer authorID) throws SQLException, ConstantException;

    Tattoo searchByID(Integer identity) throws SQLException, ConstantException;

    Integer addTattoo(Tattoo tattoo) throws SQLException, ConstantException;

    void setDate(Tattoo tattoo) throws SQLException, ConstantException;

    void deleteByID(Integer identity) throws SQLException, ConstantException;
}

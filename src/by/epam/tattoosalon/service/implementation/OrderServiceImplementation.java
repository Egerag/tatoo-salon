package by.epam.tattoosalon.service.implementation;

import by.epam.tattoosalon.dao.connection.PoolConnection;
import by.epam.tattoosalon.dao.requests.OrderDAOImplementation;
import by.epam.tattoosalon.entity.Order;
import by.epam.tattoosalon.entity.Tattoo;
import by.epam.tattoosalon.exceptions.ConstantException;
import by.epam.tattoosalon.service.OrderService;

import java.sql.SQLException;
import java.util.List;

public class OrderServiceImplementation implements OrderService {
    @Override
    public List<Tattoo> takeAllTattoos(Integer orderID) throws SQLException, ConstantException {
        OrderDAOImplementation orderDAO = new OrderDAOImplementation();
        try {
            orderDAO.setConnection();
            return orderDAO.findAllTattoos(orderID);
        } finally {
            orderDAO.closeConnection();
        }
    }

    @Override
    public Integer calculateTotalPrice(Integer orderID) throws SQLException, ConstantException {
        OrderDAOImplementation orderDAO = new OrderDAOImplementation();
        try {
            orderDAO.setConnection();
            return orderDAO.countTotalPrice(orderID);
        } finally {
            orderDAO.closeConnection();
        }
    }

    @Override
    public List<Order> searchByUsers(Integer userID) throws SQLException, ConstantException {
        OrderDAOImplementation orderDAO = new OrderDAOImplementation();
        try {
            orderDAO.setConnection();
            return orderDAO.findByUser(userID);
        } finally {
            orderDAO.closeConnection();
        }
    }

    @Override
    public Order searchActiveOrder(Integer userID) throws SQLException, ConstantException {
        OrderDAOImplementation orderDAO = new OrderDAOImplementation();
        try {
            orderDAO.setConnection();
            return orderDAO.findActiveOrder(userID);
        } finally {
            orderDAO.closeConnection();
        }
    }

    @Override
    public List<Order> searchInactiveOrder(Integer userID) throws SQLException, ConstantException {
        OrderDAOImplementation orderDAO = new OrderDAOImplementation();
        try {
            orderDAO.setConnection();
            return orderDAO.findInactiveOrder(userID);
        } finally {
            orderDAO.closeConnection();
        }
    }

    @Override
    public Integer addOrder(Order order) throws SQLException, ConstantException {
        OrderDAOImplementation orderDAO = new OrderDAOImplementation();
        try {
            orderDAO.setConnection();
            return orderDAO.create(order);
        } finally {
            orderDAO.closeConnection();
        }
    }

    @Override
    public Integer addTattooOrder(Integer orderID, Integer tattooID) throws SQLException, ConstantException {
        OrderDAOImplementation orderDAO = new OrderDAOImplementation();
        try {
            orderDAO.setConnection();
            return orderDAO.addTattoo(orderID, tattooID);
        } finally {
            orderDAO.closeConnection();
        }
    }

    @Override
    public Order searchByID(Integer identity) throws SQLException, ConstantException {
        OrderDAOImplementation orderDAO = new OrderDAOImplementation();
        try {
            orderDAO.setConnection();
            return orderDAO.findByID(identity);
        } finally {
            orderDAO.closeConnection();
        }
    }

    @Override
    public void setData(Order order) throws SQLException, ConstantException {
        OrderDAOImplementation orderDAO = new OrderDAOImplementation();
        try {
            orderDAO.setConnection();
            orderDAO.update(order);
        } finally {
            orderDAO.closeConnection();
        }
    }

    @Override
    public void deleteByID(Integer identity) throws SQLException, ConstantException {
        OrderDAOImplementation orderDAO = new OrderDAOImplementation();
        try {
            orderDAO.setConnection();
            orderDAO.delete(identity);
        } finally {
            orderDAO.closeConnection();
        }
    }

    @Override
    public void deleteTattoo(Integer orderID, Integer tattooID) throws SQLException, ConstantException {
        OrderDAOImplementation orderDAO = new OrderDAOImplementation();
        try {
            orderDAO.setConnection();
            orderDAO.deleteTattoo(orderID, tattooID);
        } finally {
            orderDAO.closeConnection();
        }
    }
}

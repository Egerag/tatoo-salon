package by.epam.tattoosalon.service.implementation;

import by.epam.tattoosalon.dao.connection.PoolConnection;
import by.epam.tattoosalon.dao.requests.TattooDAOImplementation;
import by.epam.tattoosalon.entity.Tattoo;
import by.epam.tattoosalon.exceptions.ConstantException;
import by.epam.tattoosalon.service.TattooService;

import java.sql.SQLException;
import java.util.List;

public class TattooServiceImplementation implements TattooService {
    @Override
    public List<Tattoo> takeAllTattoos() throws SQLException, ConstantException {
        TattooDAOImplementation tattooDao = new TattooDAOImplementation();
        try {
            tattooDao.setConnection();
            return tattooDao.findAllTattoo();
        } finally {
            tattooDao.closeConnection();
        }
    }

    @Override
    public List<Tattoo> searchSmallerThan(Integer maxSize) throws SQLException, ConstantException {
        TattooDAOImplementation tattooDao = new TattooDAOImplementation();
        try {
            tattooDao.setConnection();
            return tattooDao.findSmallerThan(maxSize);
        } finally {
            tattooDao.closeConnection();
        }
    }

    @Override
    public List<Tattoo> searchCheaperThan(Integer maxPrice) throws SQLException, ConstantException {
        TattooDAOImplementation tattooDao = new TattooDAOImplementation();
        try {
            tattooDao.setConnection();
            return tattooDao.cheaperThan(maxPrice);
        } finally {
            tattooDao.closeConnection();
        }
    }

    @Override
    public List<Tattoo> searchByColor(String color) throws SQLException, ConstantException {
        TattooDAOImplementation tattooDao = new TattooDAOImplementation();
        try {
            tattooDao.setConnection();
            return tattooDao.findByColor(color);
        } finally {
            tattooDao.closeConnection();
        }
    }

    @Override
    public List<Tattoo> searchByAuthor(Integer authorID) throws SQLException, ConstantException {
        TattooDAOImplementation tattooDao = new TattooDAOImplementation();
        try {
            tattooDao.setConnection();
            return tattooDao.findByAuthor(authorID);
        } finally {
            tattooDao.closeConnection();
        }
    }

    @Override
    public Tattoo searchByID(Integer identity) throws SQLException, ConstantException {
        TattooDAOImplementation tattooDao = new TattooDAOImplementation();
        try {
            tattooDao.setConnection();
            return tattooDao.findByID(identity);
        } finally {
            tattooDao.closeConnection();
        }
    }

    @Override
    public Integer addTattoo(Tattoo tattoo) throws SQLException, ConstantException {
        TattooDAOImplementation tattooDao = new TattooDAOImplementation();
        try {
            tattooDao.setConnection();
            return tattooDao.create(tattoo);
        } finally {
            tattooDao.closeConnection();
        }
    }

    @Override
    public void setDate(Tattoo tattoo) throws SQLException, ConstantException {
        TattooDAOImplementation tattooDao = new TattooDAOImplementation();
        try {
            tattooDao.setConnection();
            tattooDao.update(tattoo);
        } finally {
            tattooDao.closeConnection();
        }
    }

    @Override
    public void deleteByID(Integer identity) throws SQLException, ConstantException {
        TattooDAOImplementation tattooDao = new TattooDAOImplementation();
        try {
            tattooDao.setConnection();
            tattooDao.delete(identity);
        } finally {
            tattooDao.closeConnection();
        }
    }
}

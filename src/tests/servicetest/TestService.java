package tests.servicetest;

import by.epam.tattoosalon.dao.connection.PoolConnection;
import by.epam.tattoosalon.entity.Balance;
import by.epam.tattoosalon.entity.Tattoo;
import by.epam.tattoosalon.entity.User;
import by.epam.tattoosalon.exceptions.ConstantException;
import by.epam.tattoosalon.service.BalanceService;
import by.epam.tattoosalon.service.TattooService;
import by.epam.tattoosalon.service.UserService;
import by.epam.tattoosalon.service.implementation.BalanceServiceImplementation;
import by.epam.tattoosalon.service.implementation.TattooServiceImplementation;
import by.epam.tattoosalon.service.implementation.UserServiceImplementation;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Stream;

public class TestService {

    @DataProvider
    public Object[][] testServiceUserID() {
        return new Object[][]{
                {1, "Garbage"},
                {2, "Collector"},
                {3, "Pre$1dent"},
                {4, "Ch1kaP1ka"}
        };
    }

    @Test(dataProvider = "testServiceUserID")
    public void testFindById(final int id,
                             final String login) throws SQLException, ConstantException {
        PoolConnection.getInstance().init(1000, 0, 10);
        UserService userService = new UserServiceImplementation();
        User user = userService.searchByID(id);
        Assert.assertEquals(user.getLogin(), login);
    }

    @DataProvider
    public Object[][] testServiceUserPersonal() {
        return new Object[][]{
                {Stream.of("Garbage", "1234").toArray(), 1},
                {Stream.of("Collector", "qwer").toArray(), 2},
                {Stream.of("Pre$1dent", "krim").toArray(), 3},
                {Stream.of("Ch1kaP1ka", "2233").toArray(), 4}
        };
    }

    @Test(dataProvider = "testServiceUserPersonal")
    public void testFindByPersonal(final Object[] objects,
                                   final Integer id) throws SQLException, ConstantException {
        PoolConnection.getInstance().init(1000, 0, 10);
        UserService userService = new UserServiceImplementation();
        List<User> user = userService.searchByPersonal((String) objects[0], (String) objects[1]);
        Assert.assertEquals(user.get(0).getIdentity(), id);
    }

    @DataProvider
    public Object[][] testServiceTattooColor() {
        return new Object[][]{
                {"black", 8},
                {"orange", 1},
                {"blue", 3},
                {"red", 1}
        };
    }

    @Test(dataProvider = "testServiceTattooColor")
    public void testFindByColor(final String color,
                                   final int size) throws SQLException, ConstantException {
        PoolConnection.getInstance().init(1000, 0, 10);
        TattooService tattooService = new TattooServiceImplementation();
        List<Tattoo> tattoos = tattooService.searchByColor(color);
        Assert.assertEquals(tattoos.size(), size);
    }

    @DataProvider
    public Object[][] testServiceBalanceOwner() {
        return new Object[][]{
                {1, 1},
                {2, 2},
                {3, 3},
                {4, 4}
        };
    }

    @Test(dataProvider = "testServiceBalanceOwner")
    public void testFindByOwner(final Integer idUser,
                                final Integer idBalance) throws SQLException, ConstantException {
        PoolConnection.getInstance().init(1000, 0, 10);
        BalanceService balanceService = new BalanceServiceImplementation();
        Balance balance = balanceService.searchByOwner(idUser);
        Assert.assertEquals(balance.getIdentity(), idBalance);
    }

}
